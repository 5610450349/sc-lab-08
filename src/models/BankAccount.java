package models;

import interfaces.Measurable;

public class BankAccount implements Measurable{
	private String id ;
	private double balance ;
	
	public BankAccount(String id, double balance){
		this.id = id;
		this.balance = balance;
	}
	
	@Override
	public double getMeasure() {
		return balance ;
	}
	
	public String toString(){
		return this.id +","+ this.balance ;
	}
}

	