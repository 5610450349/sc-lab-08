package models;

import interfaces.Measurable;

public class Data {
	public static double average(Measurable objects[]){
		double sum = 0;
		for(Measurable m:objects){
			sum+= m.getMeasure();
		}
		
		if(objects.length>0){
			return sum/objects.length;
		}
		return 0;
	}

	public static Measurable min(Measurable p, Measurable q) {
		if(p.getMeasure()>q.getMeasure()){
			return q ;
		}
		return p ;
	}
}
