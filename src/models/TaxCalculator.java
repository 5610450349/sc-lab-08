package models;

import java.util.ArrayList;

import interfaces.Taxable;

public class TaxCalculator {
	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0;
		for(Taxable tax:taxList){
			sum+= tax.getTax();
		}
		
		if(taxList.size()>0){
			return sum;
		}
		return 0;
	}
}
