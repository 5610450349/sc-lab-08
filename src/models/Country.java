package models;

import interfaces.Measurable;

public class Country implements Measurable{
	private String country ;
	private double size ;
	
	public Country(String country, double size){
		this.country = country;
		this.size = size;
	}
	
	@Override
	public double getMeasure() {
		return size ;
	}
	
	public String toString(){
		return this.country+","+this.size ;
	}
}