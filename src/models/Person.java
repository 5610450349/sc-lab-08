package models;

import interfaces.Measurable;
import interfaces.Taxable;
public class Person implements Measurable, Taxable{
	private String name;
	private double height;
		
	public Person(String name, double height){
		this.name = name;
		this.height = height;
	}
	
	@Override
	public double getMeasure(){
		return this.height;
	}
	
	@Override
	public double getTax() {
		if(this.height < 300001){
			return this.height*0.05 ;
		}
		return (300000*0.05)+(this.height-300000)*0.1;
	}
	
	public String toString(){
		return this.name+"," +this.height ;
	}
}
