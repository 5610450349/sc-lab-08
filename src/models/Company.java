package models;

import interfaces.Taxable;

public class Company implements Taxable {
	private String name;
	private double revenue;
	private double expenditure;

	public Company(String name,double revenue,double expenditure){
		this.name = name;
		this.revenue = revenue;
		this.expenditure = expenditure;
	}
	
	@Override
	public double getTax() {
		return (this.revenue-this.expenditure)*0.3;
	}
}
