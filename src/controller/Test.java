package controller;

import java.util.ArrayList;

import models.BankAccount;
import models.Country;
import models.Company;
import models.Product;
import models.TaxCalculator;
import models.Data;
import models.Person;
import interfaces.Measurable;
import interfaces.Taxable;

public class Test {
	public static void main(String[] args){
		Test test = new Test();
		test.testPerson();
		test.testMin();
		test.testTax();
	}
	
	//First answer
	public void testPerson(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("John",180);
		persons[1] = new Person("Mary",180);
		persons[2] = new Person("Rick",180);
		System.out.println("Test 1 - Average");
		System.out.print(Data.average(persons)+"\n");
	}
	
	//Second answer
	public void testMin(){
		Measurable p , q ;
		p = new Person("Nan", 6000);
		q = new Person("Pink", 500);
		System.out.println("Person : "+Data.min(p,q));
				
		System.out.println("\nTest 2 - Min");
		Measurable[] pbc = new Measurable[3];
		pbc[0] = Data.min(p,q);
		p = new BankAccount("Nan", 10000);
		q = new BankAccount("Pink", 20000);
		System.out.println("Account : "+Data.min(p,q));
		pbc[1] = Data.min(p,q);
		p = new Country("Thai", 10000000);
		q = new Country("Japan", 20000000);
		System.out.println("Country : "+Data.min(p,q));
		pbc[2] = Data.min(p,q);
		p = new Person("Pink", 2000);
		q = new Person("Nan", 16000);
		System.out.println("Person : "+Data.min(p,q));
		System.out.println("Average : "+Data.average(pbc));
	}
	
	//Third answer
	public void testTax(){
		System.out.println("\nTest 3 - Tax");
		ArrayList<Taxable> p = new ArrayList<Taxable>();
		p.add(new Person("Pink", 500000));
		p.add(new Person("Nan", 100000));
		System.out.println("Person : " + TaxCalculator.sum(p));
		
		ArrayList<Taxable> c = new ArrayList<Taxable>();
		c.add(new Company("Pizza", 1000000 , 800000));
		c.add(new Company("Ice-cream", 500000 , 90000));
		System.out.println("Company : " + TaxCalculator.sum(c));
		
		ArrayList<Taxable> pd = new ArrayList<Taxable>();
		pd.add(new Product("Pen", 500));
		pd.add(new Product("Pencil", 100));
		System.out.println("Products : " + TaxCalculator.sum(pd));
		
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		all.add(new Person("Nan", 500000));
		all.add(new Company("Ice-cream", 1000000 , 800000));
		all.add(new Product("Note-book", 30000));
		System.out.println("Alls : " + TaxCalculator.sum(all));
	}

}
